#include <stdio.h>
#include <unistd.h>

#if __STDC_NO_THREADS__
	#include "threads.h"
#else
	#include <threads.h>
#endif

#include "buffer.h"

int print_stream_async(FILE *stream)
{
	if(!stream) {
		return 1;
	}

	char c;

	while((c = fgetc(stream)) != EOF) {
		putc(c, stdout);
	}

	return 0;
}

int print_stream_block(FILE *stream)
{
	if(!stream) {
		return 1;
	}

	unsigned int i;
	buffer_t *buffer = buffer_create(512);
	buffer_map(buffer, stream);
	
	for(;;) {
		for(i = 0; i < buffer -> bytes; ++i) {
			putc(buffer -> data[i], stdout);
		}
		if(buffer -> bytes < buffer -> size) {
			break;
		}
		buffer_advance(buffer, buffer -> size);
	}

	buffer_close(buffer);

	return 0;
}

int call_printers(int argc, char **argv, int uset)
{
	int ret, i, ustdin;
	for(i = 1; i < argc; ++i) {
		FILE *fp;
		ustdin = 0;
		
		if(argc > i) {
			if(argv[i][0] == '-') {   //use stdin or argv
				ustdin = 1;
			}
		}

		if(ustdin) {
			fp = stdin;
		} else {
			fp = fopen(argv[i], "rb");
		}

		if(!fp) {
			continue;
		}

		if(uset) {
			ret |= print_stream_async(fp);
			fclose(fp);
		} else {
			ret |= print_stream_block(fp);
		}
	}
	return ret;
}

int main(int argc, char **argv)
{
	int c, i, ret = 0;
	uint8_t uset = 0, hadfname = 0;

	opterr = 0;

	while((c = getopt(argc, argv, "u")) > -1) {
		switch(c) {
		case 'u':
			uset = 1;
			setvbuf(stdin, NULL, _IONBF, 0);
			break;
		default:
			break;
		}
	}

	for(i = 1; i < argc; ++i) {
		if(argv[i][0] == '-') {
			if(argv[i][1] != '\0') {
				argv[i][0] = '\0';
			}
		} else {
			hadfname = 1;
		}
	}

	if(hadfname) {
		call_printers(argc, argv, uset);
	} else {
		char *argv_n[2] = {"", "-"};
		call_printers(2, argv_n, uset);
	}

	return ret;
}
