/* 
	Copyright (C) 2015 Kyle Dominguez <kpd@opmbx.org>

	This file is part of koreutils.

    koreutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published
	by the Free Software Foundation, at version 3 of the License.

    koreutils is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with koreutils.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int rep_print(char *strn)
{
	for(;;) {
		printf("%s\n", strn);
	}
}

int main(int argc, char **argv)
{
	if(argc < 2) {
		rep_print("y");
	} else {
		char *args;
		int len = 0, i;
		for(i = 1; i < argc; ++i) {
			len += strlen(argv[i]);
		}
		args = calloc(len + argc - 1, sizeof(*args));
		int pos = 0;
		for(i = 1; i < argc; ++i) {
			int strln = strlen(argv[i]);
			strncpy(args + pos, argv[i], strln);
			pos += strln;
			if(i < (argc - 1)) {
				strncpy(args + pos, " ", 1);
				pos += 1;
			}
		}
		rep_print(args);
	}
}
