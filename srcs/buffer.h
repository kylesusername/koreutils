/* 
	Copyright (C) 2015 Kyle Dominguez <kpd@opmbx.org>

	This file is part of buffer.

    buffer is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published
	by the Free Software Foundation, at version 3 of the License.

    buffer is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with buffer.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _STDIO_H
	#include <stdio.h>
#endif

#ifndef _STDLIB_H
	#include <stdlib.h>
#endif

#ifndef _STDINT_H
	#include <stdint.h>
#endif

typedef struct buffer_struct {
	size_t size;
	size_t pos;
	size_t bytes;
	FILE *stream;
	uint8_t *data;
} buffer_t;

/* create and alloc a buffer with size maxsize */
buffer_t *buffer_create(size_t size)
{
	buffer_t *buffer;
	buffer = calloc(1, sizeof(*buffer));

	if(buffer == NULL) {
		return NULL;
	}

	buffer -> size = size;
	buffer -> pos = 0;
	buffer -> bytes = 0;
	buffer -> stream = NULL;
	buffer -> data = NULL;

	buffer -> data = calloc(size, sizeof(*(buffer -> data)));
	if(buffer -> data == NULL) {
		return NULL;
	}

	return buffer;
}

/* Map the buffer to the given file*/
int buffer_map(buffer_t *buffer, FILE *stream)
{
	size_t status;

	if(buffer == NULL) {
		return -1;
	}

	fseek(stream, 0, SEEK_SET);
	buffer -> stream = stream;
	buffer -> pos = 0;
	
	if(buffer -> data == NULL) {
		return -1;
	}

	status = fread(buffer -> data, sizeof(*(buffer -> data)), buffer -> size, buffer -> stream);
	
	buffer -> bytes = status;

	if(status != buffer -> size) {
		if(feof(stream) != 0) {
			clearerr(stream);
			return -2;
		} if(ferror(stream) != 0) {
			clearerr(stream);
			return -3;
		}
	}

	return 0;
}

int buffer_map_file(buffer_t *buffer, const char *path)
{
	FILE *fp;

	if(buffer == NULL) {
		return -1;
	}

	fp = fopen(path, "rb");

	if(!fp) {
		return -4;
	}

	return buffer_map(buffer, fp);
}

/* Advance the buffer by n bytes */
int buffer_advance(buffer_t *buffer, size_t advance)
{
	size_t status;

	if(buffer == NULL) {
		return -1;
	}

	fseek(buffer -> stream, buffer -> pos + advance, SEEK_SET);

	status = fread(buffer -> data, sizeof(*(buffer -> data)), buffer -> size, buffer -> stream);

	buffer -> bytes = status;

	if(status != buffer -> size) {
		if(feof(buffer -> stream) != 0) {
			clearerr(buffer -> stream);
			return -2;
		}
		if(ferror(buffer -> stream) != 0) {
			clearerr(buffer -> stream);
			return -3;
		}
	}

	buffer -> pos += advance;
	return 0;
}

int buffer_recede(buffer_t *buffer, size_t recede)
{
	size_t status;

	if(buffer == NULL) {
		return -1;
	}

	if(buffer -> pos > recede) {
		fseek(buffer -> stream, buffer -> pos - recede, SEEK_SET);
	} else {
		fseek(buffer -> stream, 0L, SEEK_SET);
	}

	status = fread(buffer -> data, sizeof(*(buffer -> data)), buffer -> size, buffer -> stream);
	
	buffer -> bytes = status;

	if(status != buffer -> size) {
		if(feof(buffer -> stream) != 0) {
			clearerr(buffer -> stream);
			return -2;
		} else if(ferror(buffer -> stream) != 0) {
			clearerr(buffer -> stream);
			return -3;
		}
	}
	
	return 0;
}

/* clean up the buffer and free the memory */
int buffer_close(buffer_t *buffer)
{
	if(buffer != NULL) {
		if(buffer -> stream != NULL) {
			fclose(buffer -> stream);
		}
		if(buffer -> data != NULL) {
			free(buffer -> data);
		}
		free(buffer);
	} else {
		return -1;
	}

	return 0;
}

