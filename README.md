Koreutils
=========

My implementation of the GNU coreutils
---------------------------------------

This set of coreutils will follow these paradigms:
* Threading where threading would be advantageous
* Conform strictly to ISO standards for C99
* Use only POSIX and C standard libraries
* Use only POSIX threads
* Focus on portability through conformance to standards
* Prioritize readability over speed microoptimizations (looking at you, Duff)
* Use structured programming everywhere (avoid harmful `goto`)
* Use constant data type widths (int32_t, etc.)
* Read filesystem data in chunks and unload unnnecesary memory

The utilities will have the following features:
* Conform to POSIX standard behavior for utilities at the least
* Provide additional useful behaivor to utilities
* Have optional regex for all pattern matching

Build Requirements:
* C99 compiler
* C99 stdlib
* POSIX.1-2001 (mostly) conformant system

Commands to implement:

`arch, base64, basename, cat, chgrp, chmod, chown, chroot, cksum, comm, cp,
csplit, cut, date, dd, df, dir, dircolors, dirname, du, echo, env, expand, expr,
factor, false, fmt, fold, grep, groups, head, hostid, hostname, id, install,
join, kill, link, ln, logname, ls, md5sum, mkdir, mkfifo, mknod, mktemp, mv,
nice, nl, nohup, nproc, numfmt, od, paste, pathchk, pinky, pr, printenv,
printf, ptx, pwd, readlink, realpath, rm, rmdir, runcon, seq, sha1sum,
sha224sum, sha256sum, sha384sum, sha512sum, shred, shuf, sleep, sort, split,
stat, stdbuf, stty, sum, sync, tac, tail, tee, test, timeout, touch, tr, true,
truncate, tsort, tty, uname, unexpand, uniq, unlink, uptime, users, vdir, wc,
who, whoami, yes`


Please note that this software is currently UNLICENSED and therefore NOT FREE.

Do not assume permission to use this source until a license is added.
