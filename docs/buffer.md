	Structures defined:

`typedef struct buffer_struct {
	size_t size;
	size_t pos;
	size_t bytes;
	FILE *stream;
	uint8_t *data;
} buffer_t`

size: used as the size of the area pointed at by data
pos: used as the current position of data[0] in the stream
bytes: the guaranteed number of bytes read
stream: the stream currently read
data: the bytes being pointed to

	Functions, arguments and descriptions:

`buffer_t *buffer_create(size_t size)`

Behavior:
Will initialize and allocate a buffer_t, and return the address to it

Parameters:
the size of the buffer to be created

Return:
The pointer to the address space of the buffer

`int buffer_map(buffer_t *buffer, FILE *stream)`

Behavior:
Will map up to the first size bytes of the given FILE

Parameters:
initialized pointer to buffer_t
file to map

Return:
Status.
0 = exit success
1 = end of file error
2 = random read error


`int buffer_advance(buffer_t *buffer, size_t advance)`

Behavior:
will move up the buffer's storage by n bytes

Parameters:
initialized pointer to buffer_t
number of bytes to advance the buffer by

Return:
Status.
0 = exit success
1 = end of file error
2 = random read error

`int buffer_recede(buffer_t *buffer, size_t recede)`

Behavior:
do the opposite of buffer_advance

Parameters:
initialized pointer to buffer_t
number of bytes to recede the buffer by

Return:
Status.
0 = exit success
1 = end of file error
2 = random read error

`int buffer_close(buffer_t *buffer)`

Behavior:
will close the opened stream and free the allocated memory

Parameters:
initialized pointer to buffer_t

Return:
0 = exit success
1 = on null buffer_t pointer
