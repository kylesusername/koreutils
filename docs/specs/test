NAME

    test - evaluate expression

SYNOPSIS

    test [expression]

    [ [expression] ]

DESCRIPTION

    The test utility shall evaluate the expression and indicate the result of the evaluation by its exit status. An exit status of zero indicates that the expression evaluated as true and an exit status of 1 indicates that the expression evaluated as false.

    In the second form of the utility, which uses "[]" rather than test, the application shall ensure that the square brackets are separate arguments.

OPTIONS

    The test utility shall not recognize the "--" argument in the manner specified by guideline 10 in the Base Definitions volume of IEEE Std 1003.1-2001, Section 12.2, Utility Syntax Guidelines.

    No options shall be supported.

OPERANDS

    The application shall ensure that all operators and elements of primaries are presented as separate arguments to the test utility.

    The following primaries can be used to construct expression:

    -b  pathname
        True if pathname resolves to a file that exists and is a block special file. False if pathname cannot be resolved, or if pathname resolves to a file that exists but is not a block special file.
    -c  pathname
        True if pathname resolves to a file that exists and is a character special file. False if pathname cannot be resolved, or if pathname resolves to a file that exists but is not a character special file.
    -d  pathname
        True if pathname resolves to a file that exists and is a directory. False if pathname cannot be resolved, or if pathname resolves to a file that exists but is not a directory.
    -e  pathname
        True if pathname resolves to a file that exists. False if pathname cannot be resolved.
    -f  pathname
        True if pathname resolves to a file that exists and is a regular file. False if pathname cannot be resolved, or if pathname resolves to a file that exists but is not a regular file.
    -g  pathname
        True if pathname resolves to a file that exists and has its set-group-ID flag set. False if pathname cannot be resolved, or if pathname resolves to a file that exists but does not have its set-group-ID flag set.
    -h  pathname
        True if pathname resolves to a file that exists and is a symbolic link. False if pathname cannot be resolved, or if pathname resolves to a file that exists but is not a symbolic link. If the final component of pathname is a symlink, that symlink is not followed.
    -L  pathname
        True if pathname resolves to a file that exists and is a symbolic link. False if pathname cannot be resolved, or if pathname resolves to a file that exists but is not a symbolic link. If the final component of pathname is a symlink, that symlink is not followed.
    -n  string
        True if the length of string is non-zero; otherwise, false.
    -p  pathname
        True if pathname resolves to a file that exists and is a FIFO. False if pathname cannot be resolved, or if pathname resolves to a file that exists but is not a FIFO.
    -r  pathname
        True if pathname resolves to a file that exists and for which permission to read from the file will be granted, as defined in File Read, Write, and Creation. False if pathname cannot be resolved, or if pathname resolves to a file for which permission to read from the file will not be granted.
    -S  pathname
        True if pathname resolves to a file that exists and is a socket. False if pathname cannot be resolved, or if pathname resolves to a file that exists but is not a socket.
    -s  pathname
        True if pathname resolves to a file that exists and has a size greater than zero. False if pathname cannot be resolved, or if pathname resolves to a file that exists but does not have a size greater than zero.
    -t  file_descriptor
        True if file descriptor number file_descriptor is open and is associated with a terminal. False if file_descriptor is not a valid file descriptor number, or if file descriptor number file_descriptor is not open, or if it is open but is not associated with a terminal.
    -u  pathname
        True if pathname resolves to a file that exists and has its set-user-ID flag set. False if pathname cannot be resolved, or if pathname resolves to a file that exists but does not have its set-user-ID flag set.
    -w  pathname
        True if pathname resolves to a file that exists and for which permission to write to the file will be granted, as defined in File Read, Write, and Creation. False if pathname cannot be resolved, or if pathname resolves to a file for which permission to write to the file will not be granted.
    -x  pathname
        True if pathname resolves to a file that exists and for which permission to execute the file (or search it, if it is a directory) will be granted, as defined in File Read, Write, and Creation. False if pathname cannot be resolved, or if pathname resolves to a file for which permission to execute (or search) the file will not be granted.
    -z  string
        True if the length of string string is zero; otherwise, false.
    string
        True if the string string is not the null string; otherwise, false.
    s1 =  s2
        True if the strings s1 and s2 are identical; otherwise, false.
    s1 !=  s2
        True if the strings s1 and s2 are not identical; otherwise, false.
    n1 -eq  n2
        True if the integers n1 and n2 are algebraically equal; otherwise, false.
    n1 -ne  n2
        True if the integers n1 and n2 are not algebraically equal; otherwise, false.
    n1 -gt  n2
        True if the integer n1 is algebraically greater than the integer n2; otherwise, false.
    n1 -ge  n2
        True if the integer n1 is algebraically greater than or equal to the integer n2; otherwise, false.
    n1 -lt  n2
        True if the integer n1 is algebraically less than the integer n2; otherwise, false.
    n1 -le  n2
        True if the integer n1 is algebraically less than or equal to the integer n2; otherwise, false.
    expression1 -a  expression2
        [XSI] [Option Start]
        True if both expression1 and expression2 are true; otherwise, false. The -a binary primary is left associative. It has a higher precedence than -o. [Option End]
    expression1 -o  expression2
        [XSI] [Option Start]
        True if either expression1 or expression2 is true; otherwise, false. The -o binary primary is left associative. [Option End]

    With the exception of the -h file and -L file primaries, if a file argument is a symbolic link, test shall evaluate the expression by resolving the symbolic link and using the file referenced by the link.

    These primaries can be combined with the following operators:

    !  expression
        True if expression is false. False if expression is true.
    (  expression  )
        [XSI] [Option Start] True if expression is true. False if expression is false. The parentheses can be used to alter the normal precedence and associativity. [Option End]

    The primaries with two elements of the form:

    -primary_operator primary_operand

    are known as unary primaries. The primaries with three elements in either of the two forms:

    primary_operand -primary_operator primary_operand


    primary_operand primary_operator primary_operand

    are known as binary primaries. Additional implementation-defined operators and primary_operators may be provided by implementations. They shall be of the form - operator where the first character of operator is not a digit.

    The algorithm for determining the precedence of the operators and the return value that shall be generated is based on the number of arguments presented to test. (However, when using the "[...]" form, the right-bracket final argument shall not be counted in this algorithm.)

    In the following list, $1, $2, $3, and $4 represent the arguments presented to test:

    0 arguments:
        Exit false (1).
    1 argument:
        Exit true (0) if $1 is not null; otherwise, exit false.
    2 arguments:

            If $1 is '!', exit true if $2 is null, false if $2 is not null.

            If $1 is a unary primary, exit true if the unary test is true, false if the unary test is false.

            Otherwise, produce unspecified results.

    3 arguments:

            If $2 is a binary primary, perform the binary test of $1 and $3.

            If $1 is '!', negate the two-argument test of $2 and $3.

            [XSI] [Option Start] If $1 is '(' and $3 is ')', perform the unary test of $2. [Option End]

            Otherwise, produce unspecified results.

    4 arguments:

            If $1 is '!', negate the three-argument test of $2, $3, and $4.

            [XSI] [Option Start] If $1 is '(' and $4 is ')', perform the two-argument test of $2 and $3. [Option End]

            Otherwise, the results are unspecified.

    >4 arguments:
        The results are unspecified.

        [XSI] [Option Start] On XSI-conformant systems, combinations of primaries and operators shall be evaluated using the precedence and associativity rules described previously. In addition, the string comparison binary primaries '=' and "!=" shall have a higher precedence than any unary primary. [Option End]

STDIN

    Not used.

INPUT FILES

    None.

ENVIRONMENT VARIABLES

    The following environment variables shall affect the execution of test:

    LANG
        Provide a default value for the internationalization variables that are unset or null. (See the Base Definitions volume of IEEE Std 1003.1-2001, Section 8.2, Internationalization Variables for the precedence of internationalization variables used to determine the values of locale categories.)
    LC_ALL
        If set to a non-empty string value, override the values of all the other internationalization variables.
    LC_CTYPE
        Determine the locale for the interpretation of sequences of bytes of text data as characters (for example, single-byte as opposed to multi-byte characters in arguments).
    LC_MESSAGES
        Determine the locale that should be used to affect the format and contents of diagnostic messages written to standard error.
    NLSPATH
        [XSI] [Option Start] Determine the location of message catalogs for the processing of LC_MESSAGES . [Option End]

ASYNCHRONOUS EVENTS

    Default.

STDOUT

    Not used.

STDERR

    The standard error shall be used only for diagnostic messages.

OUTPUT FILES

    None.

EXTENDED DESCRIPTION

    None.

EXIT STATUS

    The following exit values shall be returned:

     0
        expression evaluated to true.
     1
        expression evaluated to false or expression was missing.
    >1
        An error occurred.

CONSEQUENCES OF ERRORS

    Default.

The following sections are informative.
APPLICATION USAGE

    Scripts should be careful when dealing with user-supplied input that could be confused with primaries and operators. Unless the application writer knows all the cases that produce input to the script, invocations like:

    test "$1" -a "$2"

    should be written as:

    test "$1" && test "$2"

    to avoid problems if a user supplied values such as $1 set to '!' and $2 set to the null string. That is, in cases where maximal portability is of concern, replace:

    test expr1 -a expr2

    with:

    test expr1 && test expr2

    and replace:

    test expr1 -o expr2

    with:

    test expr1 || test expr2

    but note that, in test, -a has higher precedence than -o while "&&" and "||" have equal precedence in the shell.

    Parentheses or braces can be used in the shell command language to effect grouping.

    Parentheses must be escaped when using sh; for example:

    test \( expr1 -a expr2 \) -o expr3

    This command is not always portable outside XSI-conformant systems. The following form can be used instead:

    ( test expr1 && test expr2 ) || test expr3

    The two commands:

    test "$1"
    test ! "$1"

    could not be used reliably on some historical systems. Unexpected results would occur if such a string expression were used and $1 expanded to '!', '(', or a known unary primary. Better constructs are:

    test -n "$1"
    test -z "$1"

    respectively.

    Historical systems have also been unreliable given the common construct:

    test "$response" = "expected string"

    One of the following is a more reliable form:

    test "X$response" = "Xexpected string"
    test "expected string" = "$response"

    Note that the second form assumes that expected string could not be confused with any unary primary. If expected string starts with '-', '(', '!', or even '=', the first form should be used instead. Using the preceding rules without the XSI marked extensions, any of the three comparison forms is reliable, given any input. (However, note that the strings are quoted in all cases.)

    Because the string comparison binary primaries, '=' and "!=", have a higher precedence than any unary primary in the greater than 4 argument case, unexpected results can occur if arguments are not properly prepared. For example, in:

    test -d $1 -o -d $2

    If $1 evaluates to a possible directory name of '=', the first three arguments are considered a string comparison, which shall cause a syntax error when the second -d is encountered. One of the following forms prevents this; the second is preferred:

    test \( -d "$1" \) -o \( -d "$2" \)
    test -d "$1" || test -d "$2"

    Also in the greater than 4 argument case:

    test "$1" = "bat" -a "$2" = "ball"

    syntax errors occur if $1 evaluates to '(' or '!'. One of the following forms prevents this; the third is preferred:

    test "X$1" = "Xbat" -a "X$2" = "Xball"
    test "$1" = "bat" && test "$2" = "ball"
    test "X$1" = "Xbat" && test "X$2" = "Xball"

EXAMPLES

        Exit if there are not two or three arguments (two variations):

        if [ $# -ne 2 -a $# -ne 3 ]; then exit 1; fi
        if [ $# -lt 2 -o $# -gt 3 ]; then exit 1; fi

        Perform a mkdir if a directory does not exist:

        test ! -d tempdir && mkdir tempdir

        Wait for a file to become non-readable:

        while test -r thefile
        do
            sleep 30
        done
        echo '"thefile" is no longer readable'

        Perform a command if the argument is one of three strings (two variations):

        if [ "$1" = "pear" ] || [ "$1" = "grape" ] || [ "$1" = "apple" ]
        then
            command
        fi


        case "$1" in
            pear|grape|apple) command ;;
        esac

RATIONALE

    The KornShell-derived conditional command (double bracket [[]]) was removed from the shell command language description in an early proposal. Objections were raised that the real problem is misuse of the test command ( [), and putting it into the shell is the wrong way to fix the problem. Instead, proper documentation and a new shell reserved word ( !) are sufficient.

    Tests that require multiple test operations can be done at the shell level using individual invocations of the test command and shell logicals, rather than using the error-prone -o flag of test.

    XSI-conformant systems support more than four arguments.

    XSI-conformant systems support the combining of primaries with the following constructs:

    expression1 -a expression2
        True if both expression1 and expression2 are true.
    expression1 -o expression2
        True if at least one of expression1 and expression2 are true.
    ( expression )
        True if expression is true.

    In evaluating these more complex combined expressions, the following precedence rules are used:

        The unary primaries have higher precedence than the algebraic binary primaries.

        The unary primaries have lower precedence than the string binary primaries.

        The unary and binary primaries have higher precedence than the unary string primary.

        The ! operator has higher precedence than the -a operator, and the -a operator has higher precedence than the -o operator.

        The -a and -o operators are left associative.

        The parentheses can be used to alter the normal precedence and associativity.

    The BSD and System V versions of -f are not the same. The BSD definition was:

    -f  file
        True if file exists and is not a directory.

    The SVID version (true if the file exists and is a regular file) was chosen for this volume of IEEE Std 1003.1-2001 because its use is consistent with the -b, -c, -d, and -p operands ( file exists and is a specific file type).

    The -e primary, possessing similar functionality to that provided by the C shell, was added because it provides the only way for a shell script to find out if a file exists without trying to open the file. Since implementations are allowed to add additional file types, a portable script cannot use:

    test -b foo -o -c foo -o -d foo -o -f foo -o -p foo

    to find out if foo is an existing file. On historical BSD systems, the existence of a file could be determined by:

    test -f foo -o -d foo

    but there was no easy way to determine that an existing file was a regular file. An early proposal used the KornShell -a primary (with the same meaning), but this was changed to -e because there were concerns about the high probability of humans confusing the -a primary with the -a binary operator.

    The following options were not included in this volume of IEEE Std 1003.1-2001, although they are provided by some implementations. These operands should not be used by new implementations for other purposes:

    -k  file
        True if file exists and its sticky bit is set.
    -C  file
        True if file is a contiguous file.
    -V  file
        True if file is a version file.

    The following option was not included because it was undocumented in most implementations, has been removed from some implementations (including System V), and the functionality is provided by the shell (see Parameter Expansion.

    -l  string
        The length of the string string.

    The -b, -c, -g, -p, -u, and -x operands are derived from the SVID; historical BSD does not provide them. The -k operand is derived from System V; historical BSD does not provide it.

    On historical BSD systems, test -w directory always returned false because test tried to open the directory for writing, which always fails.

    Some additional primaries newly invented or from the KornShell appeared in an early proposal as part of the conditional command ( [[]]): s1 > s2, s1 < s2, str = pattern, str != pattern, f1 -nt f2, f1 -ot f2, and f1 -ef f2. They were not carried forward into the test utility when the conditional command was removed from the shell because they have not been included in the test utility built into historical implementations of the sh utility.

    The -t file_descriptor primary is shown with a mandatory argument because the grammar is ambiguous if it can be omitted. Historical implementations have allowed it to be omitted, providing a default of 1.

FUTURE DIRECTIONS

    None.

SEE ALSO

    File Read, Write, and Creation, find

CHANGE HISTORY

    First released in Issue 2.

Issue 5

    The FUTURE DIRECTIONS section is added.

Issue 6

    The -h operand is added for symbolic links, and access permission requirements are clarified for the -r, -w, and -x operands to align with the IEEE P1003.2b draft standard.

    The normative text is reworded to avoid use of the term "must" for application requirements.

    The -L and -S operands are added for symbolic links and sockets.

    IEEE Std 1003.1-2001/Cor 1-2002, item XCU/TC1/D6/38 is applied, adding XSI margin marking and shading to a line in the OPERANDS section referring to the use of parentheses as arguments to the test utility.

    IEEE Std 1003.1-2001/Cor 2-2004, item XCU/TC2/D6/30 is applied, rewording the existence primaries for the test utility.

