NAME

    echo - write arguments to standard output

SYNOPSIS

    echo [string ...]

DESCRIPTION

    The echo utility writes its arguments to standard output, followed by a <newline>. If there are no arguments, only the <newline> is written.

OPTIONS

    The echo utility shall not recognize the "--" argument in the manner specified by Guideline 10 of the Base Definitions volume of IEEE Std 1003.1-2001, Section 12.2, Utility Syntax Guidelines; "--" shall be recognized as a string operand.

    Implementations shall not support any options.

OPERANDS

    The following operands shall be supported:

    string
        A string to be written to standard output. If the first operand is -n, or if any of the operands contain a backslash ( '\' ) character, the results are implementation-defined.

        [XSI] [Option Start] On XSI-conformant systems, if the first operand is -n, it shall be treated as a string, not an option. The following character sequences shall be recognized on XSI-conformant systems within any of the arguments:

        \a
            Write an <alert>.
        \b
            Write a <backspace>.
        \c
            Suppress the <newline> that otherwise follows the final argument in the output. All characters following the '\c' in the arguments shall be ignored.
        \f
            Write a <form-feed>.
        \n
            Write a <newline>.
        \r
            Write a <carriage-return>.
        \t
            Write a <tab>.
        \v
            Write a <vertical-tab>.
        \\
            Write a backslash character.
        \0num
            Write an 8-bit value that is the zero, one, two, or three-digit octal number num.

    [Option End]

STDIN

    Not used.

INPUT FILES

    None.

ENVIRONMENT VARIABLES

    The following environment variables shall affect the execution of echo:

    LANG
        Provide a default value for the internationalization variables that are unset or null. (See the Base Definitions volume of IEEE Std 1003.1-2001, Section 8.2, Internationalization Variables for the precedence of internationalization variables used to determine the values of locale categories.)
    LC_ALL
        If set to a non-empty string value, override the values of all the other internationalization variables.
    LC_CTYPE
        [XSI] [Option Start] Determine the locale for the interpretation of sequences of bytes of text data as characters (for example, single-byte as opposed to multi-byte characters in arguments). [Option End]
    LC_MESSAGES
        Determine the locale that should be used to affect the format and contents of diagnostic messages written to standard error.
    NLSPATH
        [XSI] [Option Start] Determine the location of message catalogs for the processing of LC_MESSAGES . [Option End]

ASYNCHRONOUS EVENTS

    Default.

STDOUT

    The echo utility arguments shall be separated by single <space>s and a <newline> shall follow the last argument. [XSI] [Option Start]  Output transformations shall occur based on the escape sequences in the input. See the OPERANDS section. [Option End]

STDERR

    The standard error shall be used only for diagnostic messages.

OUTPUT FILES

    None.

EXTENDED DESCRIPTION

    None.

EXIT STATUS

    The following exit values shall be returned:

     0
        Successful completion.
    >0
        An error occurred.

CONSEQUENCES OF ERRORS

    Default.

The following sections are informative.
APPLICATION USAGE

    It is not possible to use echo portably across all POSIX systems unless both -n (as the first argument) and escape sequences are omitted.

    The printf utility can be used portably to emulate any of the traditional behaviors of the echo utility as follows (assuming that IFS has its standard value or is unset):

        The historic System V echo and the requirements on XSI implementations in this volume of IEEE Std 1003.1-2001 are equivalent to:

        printf "%b\n" "$*"

        The BSD echo is equivalent to:

        if [ "X$1" = "X-n" ]
        then
            shift
            printf "%s" "$*"
        else
            printf "%s\n" "$*"
        fi

    New applications are encouraged to use printf instead of echo.

EXAMPLES

    None.

RATIONALE

    The echo utility has not been made obsolescent because of its extremely widespread use in historical applications. Conforming applications that wish to do prompting without <newline>s or that could possibly be expecting to echo a -n, should use the printf utility derived from the Ninth Edition system.

    As specified, echo writes its arguments in the simplest of ways. The two different historical versions of echo vary in fatally incompatible ways.

    The BSD echo checks the first argument for the string -n which causes it to suppress the <newline> that would otherwise follow the final argument in the output.

    The System V echo does not support any options, but allows escape sequences within its operands, as described for XSI implementations in the OPERANDS section.

    The echo utility does not support Utility Syntax Guideline 10 because historical applications depend on echo to echo all of its arguments, except for the -n option in the BSD version.

FUTURE DIRECTIONS

    None.

SEE ALSO

    printf

CHANGE HISTORY

    First released in Issue 2.

Issue 5

    In the OPTIONS section, the last sentence is changed to indicate that implementations "do not" support any options; in the previous issue this said "need not".

Issue 6

    The following new requirements on POSIX implementations derive from alignment with the Single UNIX Specification:

        A set of character sequences is defined as string operands.

        LC_CTYPE is added to the list of environment variables affecting echo.

        In the OPTIONS section, implementations shall not support any options.

    IEEE Std 1003.1-2001/Cor 1-2002, item XCU/TC1/D6/21 is applied, so that the echo utility can accommodate historical BSD behavior.

